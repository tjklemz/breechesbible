#!/usr/bin/env bash

if [[ $# -ne 2 ]]; then
  echo "Usage: $0 BOOK CHAPTER"
  exit 1
fi

book="$1"
chapter="$2"
wget -qO- https://biblehub.com/tr/$book/$chapter.htm | pup '.greek json{}' | jq -r '.[] | .text' | awk '{ print NR, $0 }'
