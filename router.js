function match (path, route) {
	var args = [];

	var url = route.replace(/\{(.*?)\}/g, function (match, p1) {
		args.push(p1);
		return '([^/]*?)';
	});

	var res = path.match(new RegExp('^' + url + '/?$'));

	if (res && args.length) {
		return args.map(function (arg, i) {
			return res[i+1];
		});
	}

	return !!res;
}

function router (routes) {
	var routeNames = Object.keys(routes);

	return function (pathname, req, res) {
		for (var i = 0, route, m, len = routeNames.length; i < len; ++i) {
			route = routeNames[i];
			m = match(pathname, route);

			if (m) {
				routes[route].apply(null, [req, res].concat(m));
				return true;
			}
		}

		return false;
	}
}

module.exports = router;
