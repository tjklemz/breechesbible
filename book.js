var fs = require('fs');
var meta = require('./book-meta');

var isLine = function (line) {
	return line && line != '\n';
};

var lines = fs.readFileSync('GenevaBible1560.txt').toString().split('\n').filter(isLine);

var books = {};
var currentBook;
var currentChapter;
var currentVerse;

var addVerse = function (text) {
	if (!currentBook || !text) {
		return;
	}

	var v = text.match(/^\d+ /);

	var previousVerse = currentVerse;

	if (v) {
		currentVerse = v[0].trim();
		text = text.slice(v.index + v[0].length);
	}

	if (!currentVerse) {
		if (currentBook != 'Psalm' && currentBook != 'Proverbs') {
			console.warn('Expected verse:', currentBook + ' ' + currentChapter, text);
			return;
		}

		currentVerse = '1';
		text = '(' + text.trim() + ')';
	}

	if (previousVerse && previousVerse != currentVerse && +previousVerse + 1 != +currentVerse) {
		console.warn('Out of order verse:', currentBook + ' ' + currentChapter + ':' + currentVerse, text);
	}

	books[currentBook] = books[currentBook] || {};
	books[currentBook][currentChapter] = books[currentBook][currentChapter] || {};
	books[currentBook][currentChapter][currentVerse] =
		(previousVerse && previousVerse == currentVerse)
			? books[currentBook][currentChapter][currentVerse] + ' ' + text.trim()
			: text.trim();
};

for (var i=0, len=lines.length; i < len; ++i) {
	let line = lines[i];
	let m = line.match(/(\d*\s?\w+) Chapter (\d+)/);
	let m2 = line.match(/ (\d+) /);

	if (m) {
		let before = line.slice(0, m.index);
		let after = line.slice(m.index + m[0].length);

		addVerse(before);

		currentBook = m[1].trim();
		currentChapter = m[2].trim();
		currentVerse = undefined;

		addVerse(after);
	} else if (m2) {
		do {
			let v1 = line.slice(0, m2.index);
			line = line.slice(m2.index).trim();

			addVerse(v1);

			m2 = line.match(/ (\d+) /);
		} while (m2);

		if (line && line.trim().length) {
			addVerse(line);
		}
	} else {
		addVerse(line);
	}
}

module.exports = Object.freeze(books);

//console.log('Genesis', 'Chapters:', Object.keys(books['Genesis']).length)
//console.log(books['1 John'])
// var wordCount = 0;

// ['Romans', '1 Corinthians', '2 Corinthians', 'Galatians', 'Ephesians', 'Philippians', 'Colossians', '1 Thessalonians', '2 Thessalonians', '1 Timothy', '2 Timothy', 'Titus', 'Philemon', 'Hebrews'].forEach(function (book) {
// 	Object.keys(books[book]).forEach(function (chapter) {
// 		Object.keys(books[book][chapter]).forEach(function (verse) {
// 			wordCount += books[book][chapter][verse].split(' ').length;
// 		});
// 	});
// });

// console.log('total:', wordCount);
//console.log(Object.keys(books), Object.keys(books).length)
