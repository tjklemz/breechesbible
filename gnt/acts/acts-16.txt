1 Κατήντησεν δὲ εἰς Δέρβην καὶ Λύστραν καὶ ἰδού, μαθητής τις ἦν ἐκεῖ ὀνόματι Τιμόθεος υἱὸς γυναικὸς τινος Ἰουδαίας πιστῆς πατρὸς δὲ Ἕλληνος
2 ὃς ἐμαρτυρεῖτο ὑπὸ τῶν ἐν Λύστροις καὶ Ἰκονίῳ ἀδελφῶν
3 τοῦτον ἠθέλησεν ὁ Παῦλος σὺν αὐτῷ ἐξελθεῖν καὶ λαβὼν περιέτεμεν αὐτὸν διὰ τοὺς Ἰουδαίους τοὺς ὄντας ἐν τοῖς τόποις ἐκείνοις· ᾔδεισαν γὰρ ἅπαντες τὸν πατὲρα αὐτοῦ ὅτι Ἕλλην ὑπῆρχεν
4 ὡς δὲ διεπορεύοντο τὰς πόλεις παρεδίδουν αὐτοῖς φυλάσσειν τὰ δόγματα τὰ κεκριμένα ὑπὸ τῶν ἀποστόλων καὶ τῶν πρεσβυτέρων τῶν ἐν Ἰερουσαλήμ
5 αἱ μὲν οὖν ἐκκλησίαι ἐστερεοῦντο τῇ πίστει καὶ ἐπερίσσευον τῷ ἀριθμῷ καθ&#39; ἡμέραν
6 διελθόντες δὲ τὴν Φρυγίαν καὶ τὴν Γαλατικὴν χώραν κωλυθέντες ὑπὸ τοῦ ἁγίου πνεύματος λαλῆσαι τὸν λόγον ἐν τῇ Ἀσίᾳ·
7 ἐλθόντες κατὰ τὴν Μυσίαν ἐπείραζον κατὰ τὴν Βιθυνίαν πορεύεσθαι· καὶ οὐκ εἴασεν αὐτοὺς τὸ πνεῦμα
8 παρελθόντες δὲ τὴν Μυσίαν κατέβησαν εἰς Τρῳάδα
9 καὶ ὅραμα διὰ τῆς νυκτὸς ὤφθη τῷ Παύλῳ ἀνὴρ τις ἦν Μακεδών ἑστὼς παρακαλῶν αὐτὸν καὶ λέγων, Διαβὰς εἰς Μακεδονίαν βοήθησον ἡμῖν
10 ὡς δὲ τὸ ὅραμα εἶδεν εὐθέως ἐζητήσαμεν ἐξελθεῖν εἰς τὴν Μακεδονίαν συμβιβάζοντες ὅτι προσκέκληται ἡμᾶς ὁ Κύριος εὐαγγελίσασθαι αὐτούς
11 Ἀναχθέντες οὖν ἀπὸ τὴς Τρῳάδος εὐθυδρομήσαμεν εἰς Σαμοθρᾴκην τῇ τε ἐπιούσῃ εἰς Νεάπολιν
12 ἐκειθέν τε εἰς Φιλίππους ἥτις ἐστὶν πρώτη τῆς μερίδος τὴς Μακεδονίας πόλις κολωνία ἦμεν δὲ ἐν ταύτῃ τῇ πόλει διατρίβοντες ἡμέρας τινάς
13 τῇ τε ἡμέρᾳ τῶν σαββάτων ἐξήλθομεν ἔξω τῆς πόλεως παρὰ ποταμὸν οὗ ἐνομίζετο προσευχή εἶναι καὶ καθίσαντες ἐλαλοῦμεν ταῖς συνελθούσαις γυναιξίν
14 καί τις γυνὴ ὀνόματι Λυδία πορφυρόπωλις πόλεως Θυατείρων σεβομένη τὸν θεόν ἤκουεν ἡς ὁ κύριος διήνοιξεν τὴν καρδίαν προσέχειν τοῖς λαλουμένοις ὑπὸ τοῦ Παύλου
15 ὡς δὲ ἐβαπτίσθη καὶ ὁ οἶκος αὐτῆς παρεκάλεσεν λέγουσα Εἰ κεκρίκατέ με πιστὴν τῷ κυρίῳ εἶναι εἰσελθόντες εἰς τὸν οἶκόν μου μείνατε καὶ παρεβιάσατο ἡμᾶς
16 Ἐγένετο δὲ πορευομένων ἡμῶν εἰς προσευχὴν παιδίσκην τινὰ ἔχουσαν πνεῦμα Πύθωνος ἀπαντῆσαι ἡμῖν ἥτις ἐργασίαν πολλὴν παρεῖχεν τοῖς κυρίοις αὐτῆς μαντευομένη
17 αὕτη κατακολουθήσασα τῷ Παύλῳ καὶ ἡμῖν ἔκραζεν λέγουσα Οὗτοι οἱ ἄνθρωποι δοῦλοι τοῦ θεοῦ τοῦ ὑψίστου εἰσίν οἵτινες καταγγέλλουσιν ἡμῖν, ὁδὸν σωτηρίας
18 τοῦτο δὲ ἐποίει ἐπὶ πολλὰς ἡμέρας διαπονηθεὶς δὲ ὁ Παῦλος καὶ ἐπιστρέψας τῷ πνεύματι εἶπεν Παραγγέλλω σοι ἐν τῷ ὀνόματι Ἰησοῦ Χριστοῦ ἐξελθεῖν ἀπ&#39; αὐτῆς· καὶ ἐξῆλθεν αὐτῇ τῇ ὥρᾳ
19 ἰδόντες δὲ οἱ κύριοι αὐτῆς ὅτι ἐξῆλθεν ἡ ἐλπὶς τῆς ἐργασίας αὐτῶν ἐπιλαβόμενοι τὸν Παῦλον καὶ τὸν Σιλᾶν εἵλκυσαν εἰς τὴν ἀγορὰν ἐπὶ τοὺς ἄρχοντας
20 καὶ προσαγαγόντες αὐτοὺς τοῖς στρατηγοῖς εἶπον, Οὗτοι οἱ ἄνθρωποι ἐκταράσσουσιν ἡμῶν τὴν πόλιν Ἰουδαῖοι ὑπάρχοντες
21 καὶ καταγγέλλουσιν ἔθη ἃ οὐκ ἔξεστιν ἡμῖν παραδέχεσθαι οὐδὲ ποιεῖν Ῥωμαίοις οὖσιν
22 καὶ συνεπέστη ὁ ὄχλος κατ&#39; αὐτῶν καὶ οἱ στρατηγοὶ περιρρήξαντες αὐτῶν τὰ ἱμάτια ἐκέλευον ῥαβδίζειν
23 πολλάς τε ἐπιθέντες αὐτοῖς πληγὰς ἔβαλον εἰς φυλακήν παραγγείλαντες τῷ δεσμοφύλακι ἀσφαλῶς τηρεῖν αὐτούς
24 ὃς παραγγελίαν τοιαύτην εἰληφως ἔβαλεν αὐτοὺς εἰς τὴν ἐσωτέραν φυλακὴν καὶ τοὺς πόδας αὐτῶν ἠσφαλίσατο εἰς τὸ ξύλον
25 Κατὰ δὲ τὸ μεσονύκτιον Παῦλος καὶ Σιλᾶς προσευχόμενοι ὕμνουν τὸν θεόν ἐπηκροῶντο δὲ αὐτῶν οἱ δέσμιοι·
26 ἄφνω δὲ σεισμὸς ἐγένετο μέγας ὥστε σαλευθῆναι τὰ θεμέλια τοῦ δεσμωτηρίου· ἀνεῴχθησαν τε παραχρῆμα αἱ θύραι πᾶσαι καὶ πάντων τὰ δεσμὰ ἀνέθη
27 ἔξυπνος δὲ γενόμενος ὁ δεσμοφύλαξ καὶ ἰδὼν ἀνεῳγμένας τὰς θύρας τῆς φυλακῆς σπασάμενος μάχαιραν ἔμελλεν ἑαυτὸν ἀναιρεῖν νομίζων ἐκπεφευγέναι τοὺς δεσμίους
28 ἐφώνησεν δὲ φωνῇ μεγάλῃ ὁ Παῦλος λέγων, Μηδὲν πράξῃς σεαυτῷ κακόν ἅπαντες γάρ ἐσμεν ἐνθάδε
29 αἰτήσας δὲ φῶτα εἰσεπήδησεν καὶ ἔντρομος γενόμενος προσέπεσεν τῷ Παύλῳ καὶ τῷ Σιλᾷ
30 καὶ προαγαγὼν αὐτοὺς ἔξω ἔφη Κύριοι τί με δεῖ ποιεῖν ἵνα σωθῶ
31 οἱ δὲ εἶπον, Πίστευσον ἐπὶ τὸν κύριον Ἰησοῦν Χριστὸν, καὶ σωθήσῃ σὺ καὶ ὁ οἶκός σου
32 καὶ ἐλάλησαν αὐτῷ τὸν λόγον τοῦ κυρίου καὶ πᾶσιν τοῖς ἐν τῇ οἰκίᾳ αὐτοῦ
33 καὶ παραλαβὼν αὐτοὺς ἐν ἐκείνῃ τῇ ὥρᾳ τῆς νυκτὸς ἔλουσεν ἀπὸ τῶν πληγῶν καὶ ἐβαπτίσθη αὐτὸς καὶ οἱ αὐτοῦ πάντες παραχρῆμα
34 ἀναγαγών τε αὐτοὺς εἰς τὸν οἶκον αὑτοῦ, παρέθηκεν τράπεζαν καὶ ἠγαλλιάσατο πανοικὶ πεπιστευκὼς τῷ θεῷ
35 Ἡμέρας δὲ γενομένης ἀπέστειλαν οἱ στρατηγοὶ τοὺς ῥαβδούχους λέγοντες Ἀπόλυσον τοὺς ἀνθρώπους ἐκείνους
36 ἀπήγγειλεν δὲ ὁ δεσμοφύλαξ τοὺς λόγους τούτους πρὸς τὸν Παῦλον ὅτι ἀπεστάλκασιν οἱ στρατηγοὶ ἵνα ἀπολυθῆτε· νῦν οὖν ἐξελθόντες πορεύεσθε ἐν εἰρήνῃ
37 ὁ δὲ Παῦλος ἔφη πρὸς αὐτούς Δείραντες ἡμᾶς δημοσίᾳ ἀκατακρίτους ἀνθρώπους Ῥωμαίους ὑπάρχοντας ἔβαλον εἰς φυλακήν καὶ νῦν λάθρᾳ ἡμᾶς ἐκβάλλουσιν οὐ γάρ ἀλλὰ ἐλθόντες αὐτοὶ ἡμᾶς ἐξαγαγέτωσαν
38 ἀνήγγειλάν δὲ τοῖς στρατηγοῖς οἱ ῥαβδοῦχοι τὰ ῥήματα ταῦτα καὶ ἐφοβήθησαν ἀκούσαντες ὅτι Ῥωμαῖοί εἰσιν
39 καὶ ἐλθόντες παρεκάλεσαν αὐτούς καὶ ἐξαγαγόντες ἠρώτων ἐξελθεῖν τῆς πόλεως
40 ἐξελθόντες δὲ ἐκ τῆς φυλακῆς εἰσῆλθον εἰς τὴν Λυδίαν καὶ ἰδόντες τοὺς ἀδελφοὺς παρεκάλεσαν αὐτοῦς, καὶ ἐξῆλθον
