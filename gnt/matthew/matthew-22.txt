1 Καὶ ἀποκριθεὶς ὁ Ἰησοῦς πάλιν εἶπεν αὐτοῖς ἐν παραβολαῖς λέγων
2 Ὡμοιώθη ἡ βασιλεία τῶν οὐρανῶν ἀνθρώπῳ βασιλεῖ ὅστις ἐποίησεν γάμους τῷ υἱῷ αὐτοῦ
3 καὶ ἀπέστειλεν τοὺς δούλους αὐτοῦ καλέσαι τοὺς κεκλημένους εἰς τοὺς γάμους καὶ οὐκ ἤθελον ἐλθεῖν
4 πάλιν ἀπέστειλεν ἄλλους δούλους λέγων, Εἴπατε τοῖς κεκλημένοις Ἰδού, τὸ ἄριστόν μου ἡτοίμασα, οἱ ταῦροί μου καὶ τὰ σιτιστὰ τεθυμένα καὶ πάντα ἕτοιμα· δεῦτε εἰς τοὺς γάμους
5 οἱ δὲ ἀμελήσαντες ἀπῆλθον ὁ μὲν εἰς τὸν ἴδιον ἀγρόν, ὁ δὲ εἰς τὴν ἐμπορίαν αὐτοῦ·
6 οἱ δὲ λοιποὶ κρατήσαντες τοὺς δούλους αὐτοῦ ὕβρισαν καὶ ἀπέκτειναν
7 ἀκούσας δὲ ὁ βασιλεὺς ὠργίσθη καὶ πέμψας τὰ στρατεύματα αὐτοῦ ἀπώλεσεν τοὺς φονεῖς ἐκείνους καὶ τὴν πόλιν αὐτῶν ἐνέπρησεν
8 τότε λέγει τοῖς δούλοις αὐτοῦ Ὁ μὲν γάμος ἕτοιμός ἐστιν οἱ δὲ κεκλημένοι οὐκ ἦσαν ἄξιοι·
9 πορεύεσθε οὖν ἐπὶ τὰς διεξόδους τῶν ὁδῶν καὶ ὅσους ἂν εὕρητε καλέσατε εἰς τοὺς γάμους
10 καὶ ἐξελθόντες οἱ δοῦλοι ἐκεῖνοι εἰς τὰς ὁδοὺς συνήγαγον πάντας ὅσους εὗρον πονηρούς τε καὶ ἀγαθούς· καὶ ἐπλήσθη ὁ γάμος ἀνακειμένων
11 εἰσελθὼν δὲ ὁ βασιλεὺς θεάσασθαι τοὺς ἀνακειμένους εἶδεν ἐκεῖ ἄνθρωπον οὐκ ἐνδεδυμένον ἔνδυμα γάμου
12 καὶ λέγει αὐτῷ Ἑταῖρε πῶς εἰσῆλθες ὧδε μὴ ἔχων ἔνδυμα γάμου ὁ δὲ ἐφιμώθη
13 τότε εἶπεν ὁ βασιλεὺς τοῖς διακόνοις Δήσαντες αὐτοῦ πόδας καὶ χεῖρας ἄρατε αὐτὸν καὶ ἐκβάλετε εἰς τὸ σκότος τὸ ἐξώτερον· ἐκεῖ ἔσται ὁ κλαυθμὸς καὶ ὁ βρυγμὸς τῶν ὀδόντων
14 πολλοὶ γάρ εἰσιν κλητοὶ ὀλίγοι δὲ ἐκλεκτοί
15 Τότε πορευθέντες οἱ Φαρισαῖοι συμβούλιον ἔλαβον ὅπως αὐτὸν παγιδεύσωσιν ἐν λόγῳ
16 καὶ ἀποστέλλουσιν αὐτῷ τοὺς μαθητὰς αὐτῶν μετὰ τῶν Ἡρῳδιανῶν λέγοντες Διδάσκαλε οἴδαμεν ὅτι ἀληθὴς εἶ καὶ τὴν ὁδὸν τοῦ θεοῦ ἐν ἀληθείᾳ διδάσκεις καὶ οὐ μέλει σοι περὶ οὐδενός· οὐ γὰρ βλέπεις εἰς πρόσωπον ἀνθρώπων
17 εἰπὲ οὖν ἡμῖν τί σοι δοκεῖ· ἔξεστιν δοῦναι κῆνσον Καίσαρι ἢ οὔ
18 γνοὺς δὲ ὁ Ἰησοῦς τὴν πονηρίαν αὐτῶν εἶπεν Τί με πειράζετε ὑποκριταί
19 ἐπιδείξατέ μοι τὸ νόμισμα τοῦ κήνσου οἱ δὲ προσήνεγκαν αὐτῷ δηνάριον
20 καὶ λέγει αὐτοῖς Τίνος ἡ εἰκὼν αὕτη καὶ ἡ ἐπιγραφή
21 λέγουσιν αὐτῷ Καίσαρος τότε λέγει αὐτοῖς Ἀπόδοτε οὖν τὰ Καίσαρος Καίσαρι καὶ τὰ τοῦ θεοῦ τῷ θεῷ
22 καὶ ἀκούσαντες ἐθαύμασαν καὶ ἀφέντες αὐτὸν ἀπῆλθον
23 Ἐν ἐκείνῃ τῇ ἡμέρᾳ προσῆλθον αὐτῷ Σαδδουκαῖοι οἵ λέγοντες μὴ εἶναι ἀνάστασιν καὶ ἐπηρώτησαν αὐτὸν
24 λέγοντες, Διδάσκαλε, Μωσῆς εἶπεν, Ἐάν τις ἀποθάνῃ μὴ ἔχων τέκνα ἐπιγαμβρεύσει ὁ ἀδελφὸς αὐτοῦ τὴν γυναῖκα αὐτοῦ καὶ ἀναστήσει σπέρμα τῷ ἀδελφῷ αὐτοῦ
25 ἦσαν δὲ παρ&#39; ἡμῖν ἑπτὰ ἀδελφοί· καὶ ὁ πρῶτος γάμησας ἐτελεύτησεν καὶ μὴ ἔχων σπέρμα ἀφῆκεν τὴν γυναῖκα αὐτοῦ τῷ ἀδελφῷ αὐτοῦ·
26 ὁμοίως καὶ ὁ δεύτερος καὶ ὁ τρίτος ἕως τῶν ἑπτά
27 ὕστερον δὲ πάντων ἀπέθανεν καὶ ἡ γυνή
28 ἐν τῇ οὖν ἀναστάσει τίνος τῶν ἑπτὰ ἔσται γυνή πάντες γὰρ ἔσχον αὐτήν·
29 ἀποκριθεὶς δὲ ὁ Ἰησοῦς εἶπεν αὐτοῖς Πλανᾶσθε μὴ εἰδότες τὰς γραφὰς μηδὲ τὴν δύναμιν τοῦ θεοῦ·
30 ἐν γὰρ τῇ ἀναστάσει οὔτε γαμοῦσιν οὔτε ἐκγαμίζονται, ἀλλ&#39; ὡς ἄγγελοι τοῦ Θεοῦ ἐν οὐρανῷ εἰσιν
31 περὶ δὲ τῆς ἀναστάσεως τῶν νεκρῶν οὐκ ἀνέγνωτε τὸ ῥηθὲν ὑμῖν ὑπὸ τοῦ θεοῦ λέγοντος
32 Ἐγώ εἰμι ὁ θεὸς Ἀβραὰμ καὶ ὁ θεὸς Ἰσαὰκ καὶ ὁ θεὸς Ἰακώβ. οὐκ ἔστιν ὁ θεὸς Θεὸς νεκρῶν ἀλλὰ ζώντων
33 καὶ ἀκούσαντες οἱ ὄχλοι ἐξεπλήσσοντο ἐπὶ τῇ διδαχῇ αὐτοῦ
34 Οἱ δὲ Φαρισαῖοι ἀκούσαντες ὅτι ἐφίμωσεν τοὺς Σαδδουκαίους συνήχθησαν ἐπὶ τὸ αὐτό
35 καὶ ἐπηρώτησεν εἷς ἐξ αὐτῶν νομικὸς πειράζων αὐτὸν καὶ λέγων
36 Διδάσκαλε, ποία ἐντολὴ μεγάλη ἐν τῷ νόμῳ;
37 ὁ δὲ Ἰησοῦς εἶπεν αὐτῷ, Ἀγαπήσεις κύριον τὸν θεόν σου ἐν ὅλῃ τῇ καρδίᾳ σου καὶ ἐν ὅλῃ τῇ ψυχῇ σου καὶ ἐν ὅλῃ τῇ διανοίᾳ σου·
38 αὕτη ἐστὶν πρώτη καὶ μεγάλη ἐντολή
39 δευτέρα δὲ ὁμοία αὐτῇ Ἀγαπήσεις τὸν πλησίον σου ὡς σεαυτόν
40 ἐν ταύταις ταῖς δυσὶν ἐντολαῖς ὅλος ὁ νόμος καὶ οἱ προφῆται κρέμανται
41 Συνηγμένων δὲ τῶν Φαρισαίων ἐπηρώτησεν αὐτοὺς ὁ Ἰησοῦς
42 λέγων, Τί ὑμῖν δοκεῖ περὶ τοῦ Χριστοῦ τίνος υἱός ἐστιν λέγουσιν αὐτῷ Τοῦ Δαβίδ
43 λέγει αὐτοῖς Πῶς οὖν Δαβὶδ ἐν πνεύματι κύριον αὐτὸν καλεῖ λέγων
44 Εἶπεν ὁ κύριος τῷ κυρίῳ μου· Κάθου ἐκ δεξιῶν μου ἕως ἂν θῶ τοὺς ἐχθρούς σου ὑποπόδιον τῶν ποδῶν σου
45 εἰ οὖν Δαβὶδ καλεῖ αὐτὸν κύριον πῶς υἱὸς αὐτοῦ ἐστιν
46 καὶ οὐδεὶς ἐδύνατο αὐτῷ ἀποκριθῆναι λόγον οὐδὲ ἐτόλμησέν τις ἀπ&#39; ἐκείνης τῆς ἡμέρας ἐπερωτῆσαι αὐτὸν οὐκέτι
