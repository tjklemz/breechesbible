var chai = require('chai');
var assert = chai.assert;

var fs = require('fs');

var Typo = require('typo-js');
var dict = new Typo('en_US');

var bible = require('../book');

const bookNames = require('../book-meta');

var misspelledWords = {};

describe('Book', function() {
	after(function () {
		var words = Object.keys(misspelledWords).filter(function (word) {
			return misspelledWords[word] < 2;
		});
		fs.writeFileSync('./words.txt', words.join('\n'));
	});

	it('should have exactly 66 books', function () {
		var books = Object.keys(bible);
		assert.equal(books.length, 66);

		var names = Object.keys(bookNames);

		books.forEach(function (name, i) {
			assert.equal(name, names[i]);
		});
	});

	Object.keys(bookNames).forEach(function (book) {
		describe(book, function () {
			it('should have the right amount of chapters', function () {
				assert(Object.keys(bible[book]).length === bookNames[book].numChapters, 'incorrect amount of chapters for ' + book);
			});

			it('should have the right amount of verses for each chapter', function () {
				Object.keys(bible[book]).forEach(function (chapter) {
					var chapterLength = Object.keys(bible[book][chapter]).length;
					var expectedVerseNum = bookNames[book].versesPerChapter[+chapter - 1];

					assert.equal(chapterLength, expectedVerseNum, 'incorrect amount of verses for ' + book + ' Chapter ' + chapter);
				});
			});

			it('should have everything spelled correctly', function () {
				Object.keys(bible[book]).forEach(function (chapter) {
					Object.keys(bible[book][chapter]).forEach(function (verse) {
						var words = bible[book][chapter][verse].replace(/[,.!()\[\]?:;]/g, '').split(' ');
						words.forEach(function (word) {
							if (!isNaN(+word)) {
								if (!(book == 'Numbers' && chapter == 30) && !(book == '1 Samuel' && (chapter == 20 || chapter == 23 || chapter == 24))) {
									assert(false, 'misspelled word "' + word + '" in ' + book + ' ' + chapter + ':' + verse)
								}
							}
							if (!dict.check(word)) {
								misspelledWords[word] = (misspelledWords[word] || 0) + 1;
							}
							//assert(dict[word.toLowerCase()] || dict[shortWord], 'misspelled word "' + word + '" in ' + book + ' ' + chapter + ':' + verse);
						});
					});
				});
			});
		});
	});
});
