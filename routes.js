const bible = require('./book');
const greek = require('./greek');
const meta = require('./book-meta');

const bookNames = {
	'genesis': 'Genesis',
	'exodus': 'Exodus',
	'leviticus': 'Leviticus',
	'numbers': 'Numbers',
	'deutoronomy': 'Deuteronomy',
	'joshua': 'Joshua',
	'judges': 'Judges',
	'ruth': 'Ruth',
	'1samuel': '1 Samuel',
	'2samuel': '2 Samuel',
	'1kings': '1 Kings',
	'2kings': '2 Kings',
	'1chronicles': '1 Chronicles',
	'2chronicles': '2 Chronicles',
	'ezra': 'Ezra',
	'nehemiah': 'Nehemiah',
	'esther': 'Esther',
	'job': 'Job',
	'psalms': 'Psalm',
	'proverbs': 'Proverbs',
	'ecclesiastes': 'Ecclesiastes',
	'songs': 'Songs',
	'isaiah': 'Isaiah',
	'jeremiah': 'Jeremiah',
	'lamentations': 'Lamentations',
	'ezekiel': 'Ezekiel',
	'daniel': 'Daniel',
	'hosea': 'Hosea',
	'joel': 'Joel',
	'amos': 'Amos',
	'obadiah': 'Obadiah',
	'jonah': 'Jonah',
	'micah': 'Micah',
	'nahum': 'Nahum',
	'habbakuk': 'Habakkuk',
	'zephaniah': 'Zephaniah',
	'haggai': 'Haggai',
	'zechariah': 'Zechariah',
	'malachi': 'Malachi',
	'matthew': 'Matthew',
	'mark': 'Mark',
	'luke': 'Luke',
	'john': 'John',
	'acts': 'Acts',
	'romans': 'Romans',
	'1chorinthians': '1 Corinthians',
	'2corinthians': '2 Corinthians',
	'galatians': 'Galatians',
	'ephesians': 'Ephesians',
	'philippians': 'Philippians',
	'colossians': 'Colossians',
	'1thessalonians': '1 Thessalonians',
	'2thessalonians': '2 Thessalonians',
	'1timothy': '1 Timothy',
	'2timothy': '2 Timothy',
	'titus': 'Titus',
	'philemon': 'Philemon',
	'hebrews': 'Hebrews',
	'james': 'James',
	'1peter': '1 Peter',
	'2peter': '2 Peter',
	'1john': '1 John',
	'2john': '2 John',
	'3john': '3 John',
	'jude': 'Jude',
	'revelation': 'Revelation'
};

const fullNames = {
	'psalms': 'Psalms',
	'song': 'Song of Songs'
};

function json (j) {
	return JSON.stringify(j, null, 2) + '\n';
}

function getBookList (req, res) {
	const url = "/";

	var books = Object.keys(bookNames).map(function (book) {
		let fullName = fullNames[book] || bookNames[book];

		return {
			href: url + book,
			name: book,
			title: fullName
		};
	});

	res.setHeader('Content-Type', 'application/hal+json');

	return res.end(json({
		_links: {
			self: { href: url },
			book: books
		}
	}));
}

function getBook (req, res, book) {
	const url = "/" + book;

	let bookName = bookNames[book];
	let fullName = fullNames[book] || bookName;

	if (bookName) {
		res.setHeader('Content-Type', 'application/hal+json');

		let chapters = Object.keys(bible[bookName]).map(function (chapter) {
			return buildChapter(fullName, book, chapter, bible[bookName][chapter]);
		});

		res.end(json({
			_links: {
				self: { href: url, title: fullName, name: book }
			},
			name: fullName,
			reference: book,
			_embedded: {
				chapter: chapters
			}
		}));
	} else {
		res.statusCode = 404;
		res.end();
	}
}

function buildChapter (fullName, book, chapter, content) {
	const url = "/" + book + "/" + chapter;

	let verses = Object.keys(content).map(function (verse) {
		return buildVerse(fullName, book, chapter, verse, content[verse]);
	});

	return {
		_links: {
			self: { href: url, title: chapter }
		},
		reference: fullName + " " + chapter,
		number: chapter,
		_embedded: {
			verse: verses
		}
	};
}

function getChapter (req, res, book, chapter) {
	let bookName = bookNames[book];
	let fullName = fullNames[book] || bookName;
	let ch = bookName ? bible[bookName][chapter] : null;

	if (ch) {
		res.setHeader('Content-Type', 'application/hal+json');
		res.end(json(buildChapter(fullName, book, chapter, ch)));
	} else {
		res.statusCode = 404;
		res.end();
	}
}

function buildVerse (fullName, book, chapter, verse, content) {
	const reference = fullName + " " + chapter + ":" + verse;
	const url = "/" + book + "/" + chapter + "/" + verse;

	return {
		_links: {
			self: { href: url, title: reference }
		},
		reference: reference,
		number: verse,
		text: content,
		original: (function () {
			let bookName = bookNames[book];
			let fullName = fullNames[book] || bookName;
			let ch = (bookName && greek[bookName]) ? greek[bookName][chapter] : null;
			let v = ch ? ch[verse] : null;
			return v;
		})()
	};
}

function getVerse (req, res, book, chapter, verse) {
	let bookName = bookNames[book];
	let fullName = fullNames[book] || bookName;
	let ch = bookName ? bible[bookName][chapter] : null;
	let v = ch ? ch[verse] : null;

	if (v) {
		res.setHeader('Content-Type', 'application/hal+json');
		res.end(json(buildVerse(fullName, book, chapter, verse, v)));
	} else {
		res.statusCode = 404;
		res.end();
	}
}

var routes = {
	'/': getBookList,
	'/{book}': getBook,
	'/{book}/{chapter}': getChapter,
	'/{book}/{chapter}/{verse}': getVerse
};

module.exports = routes;
