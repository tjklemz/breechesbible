const fs = require('fs');
const meta = require('./book-meta');

const books = {};

Object.keys(meta).filter(bookName => !!meta[bookName].isNewTestament).forEach(bookName => {
  const name = bookName.replace(/ /g, '_').toLowerCase();

  books[bookName] = {};

  for (let chapter = 1; chapter <= meta[bookName].numChapters; chapter += 1) {
    const lines = fs.readFileSync(`gnt/${name}/${name}-${chapter}.txt`).toString().split('\n').filter(l => l && !!l.replace(/\s/g, ''));
    books[bookName][chapter] = lines.reduce((ch, verse) => {
      const v = verse.match(/^\d+ /);
      if (v) {
        let text = verse.slice(v.index + v[0].length);
        let num = Number(v);
        ch[num] = text;
      }
      return ch;
    }, {});
  }
});

module.exports = Object.freeze(books);
