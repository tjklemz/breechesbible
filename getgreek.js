const mkdirp = require('mkdirp');
const execSync = require('child_process').execSync;
const meta = require('./book-meta');

Object.keys(meta).filter(bookName => !!meta[bookName].isNewTestament).forEach(bookName => {
  const name = bookName.replace(/ /g, '_').toLowerCase();
  mkdirp.sync(`gnt/${name}`);

  for (let i = 1; i <= meta[bookName].numChapters; i += 1) {
    console.log(`${bookName} ${i}`);
    execSync(`./getgreek.sh ${name} ${i} > gnt/${name}/${name}-${i}.txt`, () => {});
  }
});
