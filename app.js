var router = require('./router');
var routes = require('./routes');
var http = require('http');
var url = require('url');

var port = process.env['PORT'] || 3000;

function createApp (router) {
	return function (req, res) {
		var pathname = url.parse(req.url).pathname;

		res.setHeader('Access-Control-Allow-Origin', '*');

		if (req.method == 'OPTIONS') {
			res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
			res.setHeader('Access-Control-Allow-Headers', 'Content-Type'); // Content-Type is needed e.g. on a POST
			res.end();
		} else if (req.method == 'GET') {
			if (!router(pathname, req, res)) {
				res.statusCode = 404;
				res.end();
			}
		} else {
			res.statusCode = 405;
			res.end();
		}
	}
}

var server = http.createServer(createApp(router(routes))).listen(port);
